""" Run the cli search tool. """

import sys
from cli.svn_comment_search import run_search, pretty_output
 
if __name__ == "__main__":
    # Usage: enter 
    # example: C:\Python26\python.exe search_svn_logs_for_redmine_hash "#5091"
    
    REDMINE_HASH = sys.argv[1]
    OUTPUT = run_search(REDMINE_HASH)
    print pretty_output(OUTPUT)