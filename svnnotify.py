# Depends on a configuration file with the following entries:
#
#   [server]
#   server=SVN_REPO_TO_MONITOR
#   user=YOUR_SVN_USERNAME
#   pass=YOUR_SVN_PASSWORD
#   last_revision=LAST_REVISION

try:
    import pysvn
    import sys, os
    sys.path.append(os.path.dirname(__file__))
except:
    print "Error while loading external depencencies."
    print "Make sure 'pysvn' is installed."
    exit()

import datetime, time, ConfigParser as cfg

def read_config(index):
    """Read the configuration file containing server, username and password"""

    config_file = 'svnnotify_%d.cfg' % index
    config_header = 'server'
    config = {}
    
    try:
        parser = cfg.ConfigParser()
        parser.read(config_file)
        config['svn_root'] = parser.get(config_header, 'server')
        config['svn_username'] = parser.get(config_header, 'user')
        config['svn_password'] = parser.get(config_header, 'pass')
        config['last_revision'] = parser.get(config_header, 'last_revision')
    except BaseException as e:
        print "Error while parsing file '%s':" % config_file
        print e
        exit()

    return config


def discover_changes(config):
    """Find out the changes occured since the last time this method is ran"""
    
    def credentials(realm, username, may_save):
        """Return the default login credentials"""
        return True, config['svn_username'], config['svn_password'], False
    
    client = pysvn.Client()
    client.callback_get_login = credentials
 
    head_revision = client.revpropget("revision", url=config['svn_root'])[0]
    config_revision = pysvn.Revision(pysvn.opt_revision_kind.number, config['last_revision'])
    
    
    if config_revision.number >= head_revision.number:
        print "At Head %s" % head_revision.number
        return None

    
    log = client.log(
        config['svn_root'], 
        discover_changed_paths=True,
        revision_end=config_revision
        )
    if len(log) is 0:
        return None

    config['last_revision'] = log[0].revision.number
        
    return log
