## Dependencies:
* Python V2.7+
* pysvn
* PyQt4 (for GUI)
* Also depends on a configuration file with the following format:

        [server]
        server=SVN_REPO_TO_MONITOR
        user=YOUR_SVN_USERNAME
        pass=YOUR_SVN_PASSWORD
        last_revision=LAST_REVISION_NUMBER_TO_MONITOR_FROM

## Usage:

To run CLI:

`python cli.py <search_term>`

To run GUI:

`python gui.py`



