"""
Search SVN logs

# Depends on a configuration file with the following entries:
#
#   [server]
#   server=SVN_REPO_TO_MONITOR
#   user=YOUR_SVN_USERNAME
#   pass=YOUR_SVN_PASSWORD
#   last_revision=LAST_REVISION
"""

try:
    import sys, os
    from collections import defaultdict
    from svnnotify import read_config, discover_changes
    # add the project's directory to the import path list.
    sys.path.append(os.path.dirname(__file__))
    
except ImportError:
    print "Error while loading external depencencies."
    print "Make sure 'svnnotify2' is installed."
    exit()

def search_svn_comments(search_term, logs):
    """
    Searches SVN logs for entries whose comments contain search_term.
    
    Returns a dict mapping filepaths -> [svn_entry] .
    """
    file_map = defaultdict(list)
    
    for entry in logs:
        if search_term not in entry.message:
            continue
        files = entry.changed_paths

        for _file in files:
            file_map[_file.path].append(entry)
            
    return file_map

def strip_path(filepath):
    """ Extract only the filename from the full path. """
    return filepath[-(filepath[::-1].find("/")):]

def pretty_output(files):
    """ Pretty prints the output dictionary from run_search. """
    output = ""
    i = 1
    for _file in sorted(files):
        output += "-" * 5 + "\n"
        output += "%s %s\n" % (str(i), _file)
        for revision in files[_file]:
            output += "\t%s %s\t%s\n" % revision
            i += 1
    return output
    
def run_search(search_term):
    """ Run a search query on the given search term.
    
    Returns a dictionary mapping files to the revisions 
    it's included in whose comments contain search_term.
    """
    config_files = [_file for _file in os.listdir(".") \
        if _file.endswith(".cfg")]
    results = defaultdict(list)
    for i in xrange(1, len(config_files)):
        config = read_config(i)
        print 'Monitoring SVN repository: %s @ revision %s' \
            % (config['svn_root'], config['last_revision'])
        print '- Press %s to quit -' % '^C'

        logs = discover_changes(config)
        
        if not logs:
            print "No log returned"
        else:
            files = search_svn_comments(search_term, logs)
            for _file in files:
                for entry in files[_file]:
                    for ch_file in entry.changed_paths:
                        if ch_file.path == _file:
                            revision = (ch_file.action, \
                                entry.revision.number, \
                                    (entry.message).replace("\n", " "))
                            results[_file].append(revision)
                            break

    return results
    
if __name__ == "__main__":
    # Usage: 
    #   example: C:\Python26\python.exe svn_comment_search "#5091"
    
    REDMINE_HASH = sys.argv[1]
    
    print pretty_output(run_search(REDMINE_HASH))
