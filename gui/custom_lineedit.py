from PyQt4 import QtGui, QtCore
class QCustomLineEdit(QtGui.QLineEdit):
    def __init__(self, parent=None):
        super(QCustomLineEdit, self).__init__(parent)
        QtCore.QObject.connect(self, \
                               QtCore.SIGNAL('textChanged(QString)'), \
                               self.onTextChanged)

        QtCore.QObject.connect(self, \
                               QtCore.SIGNAL('cursorPositionChanged(int,int)'), \
                               self.onCursorPositionChanged)

        # self.setEchoMode(QtGui.QLineEdit.Password)

        self.echoMode = self.echoMode()
        self.placeHolderText = "Enter term"
        self.isPlaceHolderActive = True

        self.setPlaceholderText("")
        self.setStyleSheet("QCustomLineEdit{color: gray;}")
        self.setEchoMode(QtGui.QLineEdit.Normal)
        self.setText(self.placeHolderText)

    def keyPressEvent(self, event):
        if self.isPlaceHolderActive:
            if event.key() == QtCore.Qt.Key_Backspace:
                event.accept()
            elif event.key() == QtCore.Qt.Key_Delete:
                self.setText("")
                self.isPlaceHolderActive = False
        QtGui.QLineEdit.keyPressEvent(self, event)

    def onCursorPositionChanged(self, _, newPos):
        if self.isPlaceHolderActive:
            if newPos != 0:
                self.setCursorPosition(0)

    def onTextChanged(self, text):
        if self.isPlaceHolderActive:
            if text != self.placeHolderText:
                self.isPlaceHolderActive = False
                temp = text
                temp = temp.mid(0, text.lastIndexOf(self.placeHolderText))
                self.setStyleSheet("QCustomLineEdit{color: black;}")
                self.setEchoMode(self.echoMode)
                self.setText(temp)
            else:
                self.setEchoMode(QtGui.QLineEdit.Normal)
                self.setText(self.placeHolderText)
                self.setStyleSheet("QCustomLineEdit{color: gray;}")
                self.setCursorPosition(0)
        else:
            if text == "":
                self.isPlaceHolderActive = True
                self.setStyleSheet("QtGui.QCustomLineEdit.Normal")
                self.setText(self.placeHolderText)





