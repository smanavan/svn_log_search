#!/usr/bin/python
# -*- coding: utf-8 -*-

""" GUI SVN log comment search interface. """

from PyQt4 import QtGui, QtCore
from cli.svn_comment_search import run_search, pretty_output
from gui.custom_lineedit import QCustomLineEdit

import logging
logging.basicConfig(filename='search_interface.log',level=logging.DEBUG)

class GUI(QtGui.QDialog):

    def __init__(self, query="", parent=None):
        """ Renders a search box and a tree view for searching and
        displaying results. 
    
        Also renders a list to filter the results in the tree. """
        super(GUI, self).__init__(parent)

	# Create layout
        self.layout = QtGui.QGridLayout()

	# Create widgets
        self.search_box = QCustomLineEdit("")
        self.file_tree = QtGui.QTreeWidget()
        self.header = ["File","Action","Revision","Comment"]
        self.file_tree.setHeaderItem(QtGui.QTreeWidgetItem(self.header))
        self.revision_list = QtGui.QListWidget()
        
	# Query entered by command line argument 
        if query != "":
            self.search_box = QtGui.QLineEdit(query)
            self.update_views()
            
	# Add widgets to layout
        self.nested_layout = QtGui.QGridLayout()
        self.nested_layout.addWidget(QtGui.QLabel("Search log:"))
        self.nested_layout.addWidget(self.search_box, 0, 1)
        self.nested_layout.addWidget(QtGui.QLabel(""), 0, 2)
        self.layout.addLayout(self.nested_layout, 0, 0)
        self.layout.addWidget(QtGui.QLabel("Filter revisions:"), 1, 1)
        self.layout.addWidget(self.file_tree, 2, 0)
        self.layout.addWidget(self.revision_list, 2, 1)
        self.setLayout(self.layout)

	# Customise layout
	self.nested_layout.setColumnMinimumWidth(2,800)
	self.nested_layout.setColumnStretch(1,0)
	self.layout.setColumnStretch(1,0)
	self.layout.setColumnStretch(0, 10)
	self.layout.setColumnMinimumWidth(0,999)
	self.layout.setRowMinimumHeight(0, 20)
	self.layout.setRowMinimumHeight(2, 20)
     
	# Connect widget signals to slots
        QtCore.QObject.connect(self.search_box, \
            QtCore.SIGNAL('returnPressed()'), self.update_views)
        QtCore.QObject.connect(self.revision_list, \
            QtCore.SIGNAL('itemClicked (QListWidgetItem *)'), \
            	self.toggle_visibility)

    def update_views(self):
        """ SLOT activated when return is pressed in the search_box.
        
        Updates the tree based on search results. """
        search_term = str(self.search_box.text())
        results = run_search(search_term)
        logging.info(pretty_output(results))
        self.clear_views()
        self.build_tree(results)
        self.resize_tree()
        
    def clear_views(self):
        """ Update the number rows of the tree. """
        self.file_tree.clear()
        self.revision_list.clear()
        
    def toggle_visibility(self, item):
        """ SLOT activated when a revision_list item is clicked.
        
        Toggles whether revisions are hidden or shown. """
        revision = item.text()
        root = self.file_tree.invisibleRootItem()
        for i in range(root.childCount()):
            for j in range(root.child(i).childCount()):
                curr = root.child(i).child(j)
                if revision == curr.text(self.header.index("Revision")):
                    if curr.isHidden():
                        curr.setHidden(False)
                        item.setBackgroundColor(QtGui.QColor(0xFFFFFF))
                    else:
                        item.setBackgroundColor(QtGui.QColor(0xCC6666))
                        curr.setHidden(True)

    def build_tree(self, results):
        """ Build the tree from search results. """
        FILE_COLUMN_FILLER = [""]
        numbers = set()
        for i, _file in enumerate(sorted(results)):
            root = QtGui.QTreeWidgetItem(self.file_tree, [_file])
            for j, (action, number, comment) in enumerate(results[_file]):
                QtGui.QTreeWidgetItem(root, \
                    FILE_COLUMN_FILLER + [action, str(number), comment])
                numbers.add(str(number))
        self.revision_list.addItems(list(numbers))
                
    def resize_tree(self):
        """ Expand and Resize the tree according to width of contents. """
        self.file_tree.expandAll()
        for i in xrange(len(self.header)):
            self.file_tree.resizeColumnToContents(i)

def main():
    """ Main function. """
    import sys
    app = QtGui.QApplication(sys.argv)
    search_term = ""
    if len(sys.argv) == 2:
        search_term = sys.argv[1]
    tree = GUI(search_term)
    tree.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
